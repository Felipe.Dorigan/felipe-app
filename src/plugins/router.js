import Vue from "vue"
import Router from "vue-router"
import Perfil from "../components/Perfil"
import Home from "../components/Home"
import Principal from "../components/Principal"
import ListaUsuario from "../components/ListaUsuario"
import ConfiguracaoPerfil from "../components/ConfiguracaoPerfil"

Vue.use(Router)

export default new Router ({
    routes: [
        {
            path: "/home", 
            name:"Home", 
            component: Home,
            icone: "mdi-home"
        },
        {
            path: "/perfil", 
            name:"Perfil", 
            component: Perfil,
            icone: "mdi-home"
        },
        {
            path: "/principal", 
            name:"Principal", 
            component: Principal,
            icone: "mdi-plus"
        },
        {
            path: "/configuracaoperfil", 
            name:"ConfiguracaoPerfil", 
            component: ConfiguracaoPerfil,
            icone: "mdi-settings",
        },
        {
            path: "/listausuario", 
            name:"Lista De Usuario", 
            component: ListaUsuario,
            icone: "mdi-truck",
        },
    ]
})
